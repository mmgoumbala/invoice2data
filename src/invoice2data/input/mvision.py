# -*- coding: utf-8 -*-




def to_text(image_path):
    """Wraps Microsoft OCR API.

    Parameters
    ----------
    path : str
        path of electronic invoice in JPG or PNG format

    Returns
    -------
    extracted_str : str
        returns extracted text from image in JPG or PNG format

    """
    import requests
    import json
    import os
    import sys
    if 'COMPUTER_VISION_SUBSCRIPTION_KEY' in os.environ:
        subscription_key = os.environ['COMPUTER_VISION_SUBSCRIPTION_KEY']
    else:
        print("\nSet the COMPUTER_VISION_SUBSCRIPTION_KEY environment variable.\n**Restart your shell or IDE for changes to take effect.**")
        sys.exit()
    # subscription_key=os.getenv('COMPUTER_VISION_SUBSCRIPTION_KEY')
    if 'COMPUTER_VISION_ENDPOINT' in os.environ:
        endpoint = os.environ['COMPUTER_VISION_ENDPOINT']
    
    ocr_url = endpoint + "vision/v2.1/ocr"
    
    

    # image_path = "static/images/4X.jpg"
    # Read the image into a byte array
    image_data = open(image_path, "rb").read()
    # Set Content-Type to octet-stream
    headers = {'Ocp-Apim-Subscription-Key': subscription_key, 'Content-Type': 'application/octet-stream'}
    params = {'language': 'unk', 'detectOrientation': 'true'}
    # put the byte array into your post request
    response = requests.post(ocr_url, headers=headers, params=params, data = image_data)

    analysis = response.json()
    # Extract the word bounding boxes and text.
    
    extracted_str = ""
    for region in analysis['regions']:
        for line in region['lines']:
            for word in line['words']:
                extracted_str = extracted_str + " " + word.get('text')

    return extracted_str

